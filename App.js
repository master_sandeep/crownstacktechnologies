import React, { useEffect } from 'react'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from 'react-native-splash-screen'
import HomeScreen from './src/pages/home';
import SongDetailScreen from './src/pages/song-details';
import { Provider as PaperProvider } from 'react-native-paper';
import { theme } from './src/styles/theme';
import { Provider } from 'react-redux';
import store from './src/Redux/store';
import ErrorBoundary from './src/components/error-boundary';

console.disableYellowBox = true;

const AppRoot = createAppContainer(
  createStackNavigator(
    {
      Home: { screen: HomeScreen },
      SongDetails: { screen: SongDetailScreen }
    }
  )
);

const App = () => {

  useEffect(() => {
    SplashScreen.hide()
  }, [])

  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <ErrorBoundary>
          <AppRoot />
        </ErrorBoundary>
      </PaperProvider>
    </Provider>
  );
}

export default App;