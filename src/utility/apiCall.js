
import { API_URL } from '../../config';

export async function apiCall(request, uri, params) {
    return (fetch(API_URL + uri, request));
}