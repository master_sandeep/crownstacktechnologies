import { Colors, DefaultTheme, Theme } from 'react-native-paper';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#47804c',
    accent: '#FE9B95',
    text: Colors.grey800,
    background: '#F5F6F7',
    lightGray: '#e5e5e5',
    black: Colors.grey800,
    white: '#FFF',
  }
};

export default theme;
