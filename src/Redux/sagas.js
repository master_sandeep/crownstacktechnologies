import { all } from 'redux-saga/effects';
import { watchHomeScreenRequests } from '../pages/home/redux/saga';
export default function* sagas() {
  yield all([
    watchHomeScreenRequests,
  ]);
}