import { combineReducers } from 'redux';
import homeReducer from "../pages/home/redux/reducer"

// Combine all the reducers
const rootReducer = combineReducers({
    home: homeReducer,
})

export default rootReducer;