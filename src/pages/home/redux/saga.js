import { put, takeLatest, call } from 'redux-saga/effects';
import { CONSTANT, homeActionCreators } from './action';
import { apiCall } from '../../../utility/apiCall';
import { makeHttpRequest } from '../../../utility/HttpsRequestMaker'

export function* fetchMoviesByGenreIdRequest(action) {
    let response;
    try {
        const request = yield call(makeHttpRequest, 'GET');
        response = yield call(apiCall, request, `search?term=Michael+jackson`);

        let status = response.status;
        response = yield response.json()
        if (status !== 200) {
            throw new Error(response);
        }
        yield put(homeActionCreators.fetchSongsSuccess(response));
    } catch (e) {
        yield put(homeActionCreators.fetchSongsError(e.message));
    }
}


export const watchHomeScreenRequests = takeLatest(CONSTANT.FETCH_SONGS_REQUEST, fetchMoviesByGenreIdRequest)

