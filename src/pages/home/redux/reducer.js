import { CONSTANT } from './action';

const initialState = {
    isFetchingSongs: false,
    songs: null,
    songsFetchingError: null
};

export default homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANT.FETCH_SONGS_REQUEST: {
            return {
                ...state,
                isFetchingSongs: true,
                songs: null,
            }
        }
        case CONSTANT.FETCH_SONGS_SUCCESS: {
            return {
                ...state,
                isFetchingSongs: false,
                songs: action.data,
            }
        }
        case CONSTANT.FETCH_SONGS_ERROR: {
            return {
                ...state,
                isFetchingSongs: false,
                songsFetchingError: action.error
            }
        }
        default:
            return state;
    }
}
