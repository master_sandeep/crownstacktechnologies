
export const CONSTANT = {
    FETCH_SONGS_REQUEST: 'FETCH_SONGS_REQUEST',
    FETCH_SONGS_SUCCESS: 'FETCH_SONGS_SUCCESS',
    FETCH_SONGS_ERROR: 'FETCH_SONGS_ERROR'
}

export const homeActionCreators = {

    fetchSongsRequest(id, page) {
        return {
            type: CONSTANT.FETCH_SONGS_REQUEST,
        }
    },

    fetchSongsSuccess(data) {
        return {
            type: CONSTANT.FETCH_SONGS_SUCCESS,
            data
        }
    },

    fetchSongsError(error) {
        return {
            type: CONSTANT.FETCH_SONGS_ERROR,
            error
        }
    }
}
