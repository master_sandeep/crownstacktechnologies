import React, { Component } from 'react';
import { TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';
import IconRender from '../../components/utility/IconRender'
import Card from '../../components/card';
import theme from '../../styles/theme';
import { homeActionCreators } from './redux/action'
import SongCard from '../../components/song-card';
import { CustomActivityIndicator } from '../../components/utility'
import { PageTitle } from '../song-details/styles'
import { Container } from '../../styles';

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { refreshing: false }
    }
    static navigationOptions = ({ navigation }) => ({
        title: 'Home',
        headerStyle: {
            height: 60,
        }
    })

    componentDidMount() {
        this.props.fetchSongsRequest()
    }


    _handleRefresh = () => {
        this.props.fetchSongsRequest()
    };

    msToHMS = (ms) => {
        // 1- Convert to seconds:
        var seconds = ms / 1000;
        // 2- Extract hours:
        var hours = parseInt(seconds / 3600); // 3,600 seconds in 1 hour
        seconds = seconds % 3600; // seconds remaining after extracting hours
        // 3- Extract minutes:
        var minutes = parseInt(seconds / 60); // 60 seconds in 1 minute

        let formate = minutes + 'm';
        if (hours) {
            formate = hours + 'h ' + formate;
        }
        return formate;
    }

    render() {
        return (
            <Container>
                {this.props.home.isFetchingSongs && <CustomActivityIndicator />}
                {this.props.home.songs &&
                    <FlatList
                        contentContainerStyle={{
                            width: '100%',
                            marginHorizontal: 20
                        }}
                        showsHorizontalScrollIndicator={false}
                        data={this.props.home.songs.results}
                        renderItem={({ item, index }) => (
                            <SongCard
                                id={item.id}
                                navigation={this.props.navigation}
                                posterPath={item.artworkUrl60}
                                loading={this.props.home.isFetchingSongs}
                                title={item.trackCensoredName}
                                artistName={item.artistName}
                                duration={this.msToHMS(item.trackTimeMillis)}
                                item={item}
                            />
                        )}
                        keyExtractor={item => 'songs' + item.trackId.toString()}
                        onRefresh={this._handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReachedThreshold={.5}
                        initialNumToRender={10}
                    />
                }
                {this.props.home.songsFetchingError &&
                    <Container>
                        <PageTitle style={{ alignItems: 'center', alignContent: 'center' }}>
                            Failed to load information
                        </PageTitle>
                    </Container>
                }
            </Container>
        );
    }
}
function mapStateToProps(state, props) {
    return {
        home: state.home
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchSongsRequest: () => dispatch(homeActionCreators.fetchSongsRequest()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
