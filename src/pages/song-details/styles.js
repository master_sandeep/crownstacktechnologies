import { Title } from 'react-native-paper';
import styled from 'styled-components/native';
import { Container } from '../../styles';
import theme from '../../styles/theme';

export const PageContainer = styled(Container)`
  background-color: ${theme.colors.white};
`;

export const PageTitle = styled(Title)``;

export const LogoContainer = styled(Container)`
  align-items: center;
  margin-top: 10px;
  max-height: 30%;
`;

export const InfoContainer = styled.View`
  flex-direction: row;
  margin-top: 10px;
  justify-content: center;
`;

export const Logo = styled.Image`
  width: 70%;
  height: 100%;
  border-radius: 6px;
  resize-mode: contain
`;

export const DetailsNameText = styled.Text`
  width: 50%;
  margin-right: 10px;
  color: ${theme.colors.accent};
  text-align: right;
`;

export const DetailsValueText = styled.Text`
  width: 50%; 
  color: ${theme.colors.primary}
`;

export const ProgressBarContainer = styled.View`
  width: 70%;
  margin-top: 15px;
`;
export const ControlButtonContainer = styled.TouchableOpacity`
  margin-horizontal: 40px
`;
export const ControlButtonText = styled.Text`
  font-size: 18px;
`;

