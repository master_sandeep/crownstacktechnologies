import React, { Component } from 'react';
import { ProgressBar, Colors } from 'react-native-paper';
import { StatusBar, Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import theme from '../../styles/theme';
import {
  Logo,
  LogoContainer,
  PageContainer,
  InfoContainer,
  DetailsNameText,
  DetailsValueText,
  ProgressBarContainer,
  ControlButtonContainer,
  ControlButtonText
} from './styles';

import TrackPlayer, {
  useTrackPlayerProgress,
} from "react-native-track-player";

function CustomProgressBar() {
  const progress = useTrackPlayerProgress();
  let progressDone = 0;
  if (progress.position) {
    progressDone = progress.position / 100;
  }
  return (
    <ProgressBarContainer >
      <ProgressBar progress={progressDone} color={Colors.red800} />
    </ProgressBarContainer>
  );
}


function ControlButton({ title, onPress }) {
  return (
    <ControlButtonContainer onPress={onPress}>
      <ControlButtonText >{title}</ControlButtonText>
    </ControlButtonContainer>
  );
}

export class SongDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      buttonTitle: "Play"
    }
  }

  static navigationOptions = ({ navigation }) => {
    return ({
      title: navigation.state.params.item.trackCensoredName,
      headerStyle: {
        height: 60,
        marginTop: 0,
        paddingTop: 10,
        paddingBottom: 30,
      },
    })
  }

  async componentDidMount() {
    await this.setup();
  }

  componentWillMount() {
    TrackPlayer.destroy();
  }

  setup = async () => {
    await TrackPlayer.setupPlayer({});
    await TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_STOP,
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE
      ]
    });
  }

  togglePlayback = async () => {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    var playbackState = await TrackPlayer.getState();
    if (currentTrack == null) {
      await TrackPlayer.reset();
      await TrackPlayer.add({
        id: this.props.songDetail.trackId,
        url: this.props.songDetail.previewUrl,
        title: this.props.songDetail.trackName,
        artist: this.props.songDetail.artistName,
        artwork: this.props.songDetail.artworkUrl100,
        duration: this.props.songDetail.trackTimeMillis
      });
      await TrackPlayer.play();
      this.setState({ buttonTitle: 'Pause' })
    } else {
      if (playbackState === TrackPlayer.STATE_PAUSED) {
        this.setState({ buttonTitle: 'Pause' })
        await TrackPlayer.play();
      } else {
        this.setState({ buttonTitle: 'Play' })
        await TrackPlayer.pause();
      }
    }
  }

  render() {
    const { songDetail } = this.props;
    var middleButtonText = this.state.buttonTitle;
    return (
      <PageContainer
        cent
      >
        <StatusBar
          barStyle="dark-content"
          backgroundColor={theme.colors.white}
        />
        <LogoContainer>
          <Logo
            source={{
              uri: songDetail.artworkUrl100
            }}
          />
        </LogoContainer>
        <InfoContainer>
          <DetailsNameText>Artist Name:</DetailsNameText>
          <DetailsValueText>{songDetail.artistName}</DetailsValueText>
        </InfoContainer>
        <InfoContainer>
          <DetailsNameText>Track Name:</DetailsNameText>
          <DetailsValueText>{songDetail.trackName}</DetailsValueText>
        </InfoContainer>
        <InfoContainer>
          <CustomProgressBar />
          <ControlButton title={middleButtonText} onPress={this.togglePlayback} />
        </InfoContainer>
      </PageContainer>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    songDetail: props.navigation.state.params.item
  }
}


export default connect(mapStateToProps)(SongDetails);