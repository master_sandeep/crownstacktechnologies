import React, { FC } from 'react';
import { CardContainer, CardTitle } from './styles';

const Card = ({
  id,
  label,
  onPress,
  selected,
  disabled
}) => {
  return (
    <CardContainer
      key={id}
      onPress={onPress}
      centered
      selected={selected}
      elevation={3}
      disabled={disabled}>
      <CardTitle selected={selected}>{label}</CardTitle>
    </CardContainer>
  );
};

export default Card;
