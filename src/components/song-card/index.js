import React from 'react';
import { Text, View } from 'react-native'
import { CardContainer, CardLogo, CardRightContainer } from './styles';
import theme from '../../styles/theme';

const SongCard = ({
  id,
  posterPath,
  loading,
  navigation,
  title,
  artistName,
  duration,
  item
}) => {
  return (
    <CardContainer
      key={id}
      onPress={() => {
        navigation.navigate('SongDetails', { item: item });
      }}
      elevation={3}
      centered
      disabled={loading}>
      <CardLogo
        source={{ uri: posterPath }}
      />
      <CardRightContainer >
        <Text numberOfLines={2} style={{ fontSize: 16, fontWeight: 'bold' }}>{title}</Text>
        <View style={{ flexDirection: 'row', marginTop: 5, }}>
          <Text numberOfLines={1} style={{ marginRight: 10, maxWidth: '50%' }}>{artistName}</Text>
          <Text style={{ color: theme.colors.black }}>{duration}</Text>
        </View>
      </CardRightContainer>
    </CardContainer>
  );
};

export default SongCard;
