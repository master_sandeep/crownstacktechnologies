import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Foundation from 'react-native-vector-icons/Foundation';

export default class IconRender extends React.Component {
    render() {
        switch (this.props.familyName) {
            case undefined: {
                return <FontAwesome name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'FontAwesome': {
                return <FontAwesome name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'MaterialIcons': {
                return <MaterialIcons name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'MaterialCommunityIcons': {
                return <MaterialCommunityIcons name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'Entypo': {
                return <Entypo name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'Feather': {
                return <Feather name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'Fontisto': {
                return <Fontisto name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'Ionicons': {
                return <Ionicons name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'AntDesign': {
                return <AntDesign name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'EvilIcons': {
                return <EvilIcons name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            case 'Foundation': {
                return <Foundation name={this.props.name} style={this.props.style} size={this.props.size} color={this.props.color}/>;
            }
            default: {
                return null
            }
        }
        // return <FontAwesome name={this.props.name} style={this.props.style} size={this.props.size} />;
    }
}