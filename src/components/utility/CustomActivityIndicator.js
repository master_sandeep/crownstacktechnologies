
import React from 'react';
import { ActivityIndicator } from 'react-native'
import { ListFooterContainer, SectionPageContainer, LoadingText } from '../../styles'
import theme from '../../styles/theme';

export default CustomActivityIndicator = (props) => {
    return (
        <SectionPageContainer centered>
            <ActivityIndicator size="large" color={'red'} />
            <LoadingText>Loading</LoadingText>
        </SectionPageContainer>
    )
}